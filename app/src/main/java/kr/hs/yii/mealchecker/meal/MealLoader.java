package kr.hs.yii.mealchecker.meal;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.Source;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by parkjongheum on 9/26/15.
 */
public class MealLoader {
    static Source source;

    /**
     * 급식을 불러옵니다.
     * @param context 데이터베이스를 열기위해 필요한 컨텍스트
     * @param ay 급식을 불러올 년
     * @param mm 급식을 불러올 월
     * @return 급식 목록
     */
    public static List<MealItem> getMeal(Context context, int ay, int mm) {
        List<MealItem> items = new ArrayList<>();
        String url = "http://stu.goe.go.kr/sts_sci_md00_001.do?schulCode=J100000744&schulCrseScCode=4&schulKndScCode=04&ay="+ay+"&mm="+String.format("%02d",mm)+"&insttNm=용인정보고등학교";
        try {
            source = new Source(new URL(url));
        } catch (Exception e){ // 모든 예외는 한방에 !
            e.printStackTrace();
        }
        source.fullSequentialParse();
        Log.d("MealLoader", url);
        List<Element> table = source.getAllElements("table");
        Log.d("MealLoader", "Parse!");
        for(int a=0; a<table.size();a++){
            if(((Element)table.get(a)).getAttributeValue("class").equals("tbl_type3 tbl_calendar")){
                List<Element> tbody = ((Element)table.get(a)).getAllElements("tbody");
                Log.d("MealLoader","tbody found!");
                for(int b=0;b<tbody.size();b++){ // table body의 갯수만큼 루프
                    List<Element> tr = ((Element)tbody.get(b)).getAllElements("tr");
                    Log.d("MealLoader","tr found!");
                    for(int c=0;c<tr.size();c++){ // table row안 갯수만큼 루프
                        List<Element> td = ((Element)tr.get(c)).getAllElements("td");
                        Log.d("MealLoader","td found!");
                        for(int d=0;d<td.size();d++){
                            String raw = ((Element)td.get(d)).getContent().toString();
                            if(raw.equals("<div> </div>")||raw.equals("<div></div>")){
                                Log.d("Meal","Empty cell");
                            } else {
                                if(raw.contains("<br />")){
                                    Log.d("Meal", "Found MealInfo");
                                    raw = raw.replace("<div>","").replace("</div>", "");
                                    String[] split = raw.split("<br />",2);
                                    split[1] = split[1].replaceFirst("<br />","");
                                    split[1] = split[1].replace("<br />","\n");
                                    split[1] = split[1].replace("[중식]","");
                                    split[1] = split[1].replaceAll("\\d","");
                                    split[1] = split[1].replace(".","").replace("*","");
                                    split[1] = split[1].replaceAll("\\(.*\\)","");
                                    split[1] = split[1].replaceAll("\\(.[^\n]*","");
                                    items.add(new MealItem(""+ay+"-"+String.format("%02d",mm)+"-"+String.format("%02d",Integer.valueOf(split[0])),split[1]));
                                } else {
                                    Log.d("Meal","Date Only");
                                }
                            }
                        }
                    }
                }
            }
        }
        return items;
    }
}
