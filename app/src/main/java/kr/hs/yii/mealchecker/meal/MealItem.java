package kr.hs.yii.mealchecker.meal;

/**
 * Created by parkjongheum on 9/26/15.
 */
public class MealItem {
    String date;
    String mealContent;

    MealItem(String date, String mealContent){
        this.date = date;
        this.mealContent = mealContent;
    }

    public String getMealContent() {
        return mealContent;
    }

    public String getDate() {
        return date;
    }
}
