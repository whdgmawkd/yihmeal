package kr.hs.yii.mealchecker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Calendar;

import kr.hs.yii.mealchecker.meal.MealDB;
import kr.hs.yii.mealchecker.meal.MealLoader;

public class MealLoadingActivity extends AppCompatActivity {

    boolean isUpdated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_loading);
        Intent intent = getIntent();
        boolean isRefresh = intent.getBooleanExtra("isRefresh",false);
        if(Calendar.getInstance().get(Calendar.DAY_OF_WEEK)<=2){
            isRefresh=true;
        }
        final Thread thread_firstRun = new Thread(new Runnable() {
            @Override
            public void run() {
                MealDB.dropAllMeal(getApplicationContext());
                MealDB.updateMealDB(getApplicationContext(), MealLoader.getMeal(getApplicationContext(),
                        Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) + 1));
                isUpdated = true;
            }
        });
        final Thread thread_nextMonth = new Thread(new Runnable() {
            @Override
            public void run() {
                MealDB.dropAllMeal(getApplicationContext());
                thread_firstRun.start();
                MealDB.updateMealDB(getApplicationContext(), MealLoader.getMeal(getApplicationContext(),
                        Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) + 2));
                isUpdated = true;
            }
        });
        if(isRefresh && isNetworkConnected(getApplicationContext())){ // if it is refresh require by user
            MealDB.dropAllMeal(getApplicationContext());
            thread_firstRun.start();
        }
        else if((Calendar.getInstance().get(Calendar.DAY_OF_MONTH)>=25 && MealDB.getCount(getApplicationContext()) <= 5)) { // 매월 말일이면 새로고침, getCount는 현제 날짜를 기준으로 개수를 구함.
            if(isNetworkConnected(getApplicationContext())) {
                MealDB.dropAllMeal(getApplicationContext());
                thread_firstRun.start();
                thread_nextMonth.start();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.network_dialog_title)
                        .setMessage(R.string.network_dialog_msg)
                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }
        } else if(MealDB.getCount(getApplicationContext())==0) { // if it is first run
            if(isNetworkConnected(getApplicationContext())) {
                MealDB.dropAllMeal(getApplicationContext());
                thread_firstRun.start();
            } else {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.network_dialog_title)
                        .setMessage(R.string.network_dialog_msg)
                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).show();
            }
        }

        Thread thread_checkState = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if ((thread_firstRun.getState() == Thread.State.NEW || thread_firstRun.getState() == Thread.State.TERMINATED) &&
                            (thread_nextMonth.getState() == Thread.State.NEW || thread_nextMonth.getState() == Thread.State.TERMINATED)) {
                        startActivity(new Intent(getApplicationContext(),MealViewerActivity.class));
                        finish();
                        break;
                    }
                }
            }
        });
        thread_checkState.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_meal_list, menu); Do not Inflate menu.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isNetworkConnected(Context context){

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if(mobile.isConnected() || wifi.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

}
