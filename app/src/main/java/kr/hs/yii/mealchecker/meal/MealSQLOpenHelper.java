package kr.hs.yii.mealchecker.meal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by parkjongheum on 9/26/15.
 */
public class MealSQLOpenHelper extends SQLiteOpenHelper {

    public MealSQLOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * Create Database
     * @param db 데이터베이스
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table meal (" +
                "mealDate text, mealContent text" +
                ");";
        db.execSQL(sql);
    }

    /**
     * version code is "yyyyMM",so if month changed, drop all table.
     * @param db 데이터베이스
     * @param oldVersion 기존 버전
     * @param newVersion 새로운 버전
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DELETE FROM meal";
        db.execSQL(sql);
    }
}
