package kr.hs.yii.mealchecker.meal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.Calendar;
import java.util.List;

/**
 * Created by parkjongheum on 9/30/15.
 */
public class MealDB {
    public static boolean updateMealDB(Context context, List<MealItem> mealItems){
        MealSQLOpenHelper helper = new MealSQLOpenHelper(context, "meal.db", null, getVersion());
        SQLiteDatabase db = helper.getWritableDatabase();
        for(int a=0;a<mealItems.size();a++){
            ContentValues values = new ContentValues();
            values.put("mealDate",mealItems.get(a).getDate());
            values.put("mealContent",mealItems.get(a).getMealContent());
            db.insert("meal",null,values);
        }
        return true;
    }
    public static int getVersion(){
        Calendar c = Calendar.getInstance();
        int version = Integer.parseInt("" + c.get(Calendar.YEAR) + String.format("%02d", c.get(Calendar.MONTH) + 1));
        return version;
    }

    public static int getCount(Context context){
        Calendar c = Calendar.getInstance();
        int cnt=0;
        MealSQLOpenHelper helper = new MealSQLOpenHelper(context,"meal.db",null,getVersion());
        SQLiteDatabase db = helper.getReadableDatabase();
        String sql = "SELECT * FROM meal WHERE Datetime(mealDate) >= '"+c.get(Calendar.YEAR)+"-"
                +String.format("%02d",c.get(Calendar.MONTH)+1)+"-"+String.format("%02d",c.get(Calendar.DAY_OF_MONTH))+"';";
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            cnt=cursor.getCount();
        }
        finally {
            cursor.close();
        }
        return cnt;
    }

    public static MealItem getMealOnPosition(Context context, int position){
        Calendar c = Calendar.getInstance();
        MealSQLOpenHelper helper = new MealSQLOpenHelper(context, "meal.db",null, getVersion());
        SQLiteDatabase db = helper.getReadableDatabase();
        String sql = "SELECT * FROM meal WHERE Datetime(mealDate) >= '"+c.get(Calendar.YEAR)+"-"
                +String.format("%02d",c.get(Calendar.MONTH)+1)+"-"+String.format("%02d",c.get(Calendar.DAY_OF_MONTH))+"' LIMIT "+position+",1 ;";
        Cursor cursor = null;
        MealItem rst = null;
        Log.d("MealDB","Receive postion : "+position);
        try {
            cursor = db.rawQuery(sql, null);
            if(cursor.moveToNext()) {
                rst = new MealItem(cursor.getString(cursor.getColumnIndex("mealDate")),cursor.getString(cursor.getColumnIndex("mealContent")));
            }
        }
        finally {
            cursor.close();
        }
        return rst;
    }

    public static void dropAllMeal(Context context){
        MealSQLOpenHelper helper = new MealSQLOpenHelper(context,"meal.db",null,getVersion());
        SQLiteDatabase db = helper.getWritableDatabase();
        db.execSQL("DELETE FROM meal");
    }
}
